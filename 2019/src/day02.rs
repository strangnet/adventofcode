use std::io::{BufRead, BufReader};
use std::fs::File;

const DAY: &str = "02";
const OPCODE_STEP: usize = 4;

fn main() {
    part01();
    part02();
}

fn part01() {
    let program_input = get_program_input();

    let res = run_program(&program_input, 12, 2);
    println!("{}", res[0]);
}

fn part02() {
    let program_input = get_program_input();

    for a in 0..100 {
        for b in 0..100 {
            let res = run_program(&program_input, a, b);
            if res[0] == 19690720 {
                println!("{}", 100 * a + b);
            }
        }
    }
}

fn get_program_input() -> Vec<usize> {
    let path = format!("inputs/day{}/input.txt", DAY);
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);

    let mut program_input: Vec<usize> = Vec::new();

    for line in reader.lines() {
        let l = line.unwrap();
        let split_str: Vec<&str> = l.split(',').collect();
        for e in split_str {
            let num = e.trim();
            let num_parse = num.parse::<usize>().unwrap();
            program_input.push(num_parse);
        }
    }

    return program_input;
}

fn run_program(input: &Vec<usize>, input1: usize, input2: usize) -> Vec<usize> {
    let mut output = input.clone();
    output[1] = input1;
    output[2] = input2;
    let mut running = true;
    let mut i = 0;
    while running && i < output.len() {
        match output[i] {
            99 => running = false,
            1 => {
                let a = output[output[i+1]];
                let b = output[output[i+2]];
                let pos = output[i+3];
                output[pos] = a + b;
                i += OPCODE_STEP;
            },
            2 => {
                let a = output[output[i+1]];
                let b = output[output[i+2]];
                let pos = output[i+3];
                output[pos] = a * b;
                i += OPCODE_STEP;
            },
            _ => running = false,
        }
    }

    return output;
}
