use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

const DAY: &str = "01";

fn main() {
    // part 1
    fuel_calculation(&estimate_fuel);

    // part 2
    fuel_calculation(&estimate_total_fuel);
}

fn fuel_calculation(fun: &dyn Fn(i32) -> i32) {
    let path = format!("inputs/day{}/01.txt", DAY);
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let mut total_fuel: i32 = 0;

    for line in reader.lines() {
        let l = line.unwrap();
        let fuel = fun(l.parse::<i32>().unwrap());
        total_fuel += fuel;
    }

    println!("Total Fuel: {}", total_fuel);
}

fn estimate_fuel(mass: i32) -> i32 {
    mass / 3 - 2
}

fn estimate_total_fuel(mass: i32) -> i32 {
    if mass <= 5 {
        return 0;
    } else {
        let fuel = mass / 3 - 2;
        return fuel + estimate_total_fuel(fuel);
    }
}
