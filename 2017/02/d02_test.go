package aoc2017d02

import (
	"testing"
)

func TestCalculateChecksum(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Testing 5 1 9 5",
			args{"5	1	9	5"},
			8,
		},
		{
			"Testing 7 5 3",
			args{"7	5	3"},
			4,
		},
		{
			"Testing 2 4 6 8",
			args{"2	4 6	8"},
			6,
		},
		{
			"Testing row 1",
			args{"515	912	619	2043	96	93	2242	1385	2110	860	2255	621	1480	118	1230	99"},
			2162,
		},
		{
			"Testing row 16",
			args{"707	668	1778	1687	2073	1892	62	1139	908	78	1885	800	945	712	57	65"},
			2016,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CalculateChecksum(tt.args.input); got != tt.want {
				t.Errorf("CalculateChecksum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCalculateChecksum2(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Testing 5 9 2 8",
			args{"5 9 2 8"},
			4,
		},
		{
			"Testing 9 4 7 3",
			args{"9 4 7 3"},
			3,
		},
		{
			"Testing 3 8 6 5",
			args{"3 8 6 5"},
			2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CalculateChecksum2(tt.args.input); got != tt.want {
				t.Errorf("CalculateChecksum2() = %v, want %v", got, tt.want)
			}
		})
	}
}
