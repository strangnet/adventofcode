package aoc2017d03

import (
	"testing"
)

func TestCountSteps(t *testing.T) {
	type args struct {
		input int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Testing 1",
			args{1},
			0,
		},
		{
			"Testing 12",
			args{12},
			3,
		},
		{
			"Testing 23",
			args{23},
			2,
		},
		{
			"Testing 1024",
			args{1024},
			31,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CountSteps(tt.args.input); got != tt.want {
				t.Errorf("CountSteps() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFindFirstThresholdValue(t *testing.T) {
	type args struct {
		threshold int
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{
			"Testing 1",
			args{1},
			1,
		},
		{
			"Testing 2",
			args{2},
			1,
		},
		{
			"Testing 3",
			args{3},
			2,
		},
		{
			"Testing 4",
			args{4},
			4,
		},
		{
			"Testing 5",
			args{5},
			5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FindFirstThresholdValue(tt.args.threshold); got != tt.want {
				t.Errorf("FindFirstThresholdValue() = %v, want %v", got, tt.want)
			}
		})
	}
}
