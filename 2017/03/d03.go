package aoc2017d03

import (
	"fmt"
)

var (
	myInput = 325489

	memoryMap  map[Coord]*GridPoint
	gridPoints map[int]*GridPoint
)

const (
	Right = iota
	Up
	Left
	Down
)

// Exercise ...
func Exercise() {
	fmt.Println("AOC 2017: Day 3")
	fmt.Println("Running puzzle 1")
	steps := CountSteps(myInput)
	fmt.Printf("  => Steps from input %d: %d\n", myInput, steps)

	fmt.Println("Running puzzle 2")
	value := FindFirstThresholdValue(myInput)
	fmt.Printf("  => Threshold value from input %d: %d\n", myInput, value)
}

func CountSteps(index int) int {
	CreateMemoryMap(index)
	last, _ := accessGridPoint(index)

	fmt.Printf("Distance for grid point with index %d, found at coord %d,%d is: %d\n", index, last.coord.x, last.coord.y, last.Distance())

	return last.Distance()
}

func FindFirstThresholdValue(threshold int) int64 {
	index := CreateMemoryMapUntilThresholdValue(threshold)
	last, _ := accessGridPoint(index)
	prev, _ := accessGridPoint(index - 1)

	fmt.Printf("Threshold (%d) from grid point %d to %d where value switched from %d to %d\n", threshold, index-1, index, prev.value, last.value)

	return last.value
}

type Coord struct {
	x int
	y int
}

type GridPoint struct {
	value int64
	index int
	coord Coord
}

func (gp *GridPoint) Distance() int {
	return Abs(gp.coord.x) + Abs(gp.coord.y)
}

func Abs(a int) int {
	if a < 0 {
		return -1 * a
	}
	return a
}

func CreateMemoryMap(toIndex int) {
	current := initMemoryMap()
	direction := Right
	for i := 1; i < toIndex; i++ {
		switch direction {
		case Down:
			if _, ok := accessCoordinate(current.coord.x+1, current.coord.y); !ok {
				direction = Right
			}
		case Right:
			if _, ok := accessCoordinate(current.coord.x, current.coord.y+1); !ok {
				direction = Up
			}
		case Up:
			if _, ok := accessCoordinate(current.coord.x-1, current.coord.y); !ok {
				direction = Left
			}
		case Left:
			if _, ok := accessCoordinate(current.coord.x, current.coord.y-1); !ok {
				direction = Down
			}
		}
		current = NewGridPointWithDirection(current, direction, false)
	}

}

func CreateMemoryMapUntilThresholdValue(value int) int {
	current := initMemoryMap()
	direction := Right
	for i := 1; i < value; i++ {
		switch direction {
		case Down:
			if _, ok := accessCoordinate(current.coord.x+1, current.coord.y); !ok {
				direction = Right
			}
		case Right:
			if _, ok := accessCoordinate(current.coord.x, current.coord.y+1); !ok {
				direction = Up
			}
		case Up:
			if _, ok := accessCoordinate(current.coord.x-1, current.coord.y); !ok {
				direction = Left
			}
		case Left:
			if _, ok := accessCoordinate(current.coord.x, current.coord.y-1); !ok {
				direction = Down
			}
		}
		current = NewGridPointWithDirection(current, direction, true)
		if current.value > int64(value) {
			break
		}
	}
	return current.index
}

// NewGridPoint returns a new grid point at coordinate x,y with index in the grid
func NewGridPoint(x int, y int, index int, value int64) *GridPoint {
	gp := &GridPoint{coord: Coord{x: x, y: y}, index: index, value: value}
	memoryMap[gp.coord] = gp
	gridPoints[gp.index] = gp
	return gp
}

func NewGridPointWithDirection(previous *GridPoint, direction int, accumulateValues bool) *GridPoint {
	x := previous.coord.x
	y := previous.coord.y
	switch direction {
	case Right:
		x++
	case Up:
		y++
	case Left:
		x--
	case Down:
		y--
	}

	var value int64
	if accumulateValues {
		value = accumulatedValues(x, y)
	}
	return NewGridPoint(x, y, previous.index+1, value)
}

func accumulatedValues(x int, y int) int64 {
	var sum int64
	for _, dx := range []int{-1, 0, 1} {
		for _, dy := range []int{-1, 0, 1} {
			if gp, ok := accessCoordinate(x+dx, y+dy); ok {
				sum += gp.value
			}
		}
	}
	fmt.Printf("Acc values for %d,%d is %d\n", x, y, sum)
	return sum
}

func accessCoordinate(x int, y int) (*GridPoint, bool) {
	gp, ok := memoryMap[Coord{x, y}]
	return gp, ok
}

func accessGridPoint(index int) (*GridPoint, bool) {
	gp, ok := gridPoints[index]
	return gp, ok
}

// initMemoryMap initializes the memory map and returns a pointer ti
// the access port, i.e index 1
func initMemoryMap() *GridPoint {
	memoryMap = make(map[Coord]*GridPoint)
	gridPoints = make(map[int]*GridPoint)
	gp := NewGridPoint(0, 0, 1, 1)
	return gp
}
