package aoc2017d01

import (
	"testing"
)

func TestSumAllDigits(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Testing 1122",
			args{input: "1122"},
			3,
		},
		{
			"Testing 1111",
			args{input: "1111"},
			4,
		},
		{
			"Testing 1234",
			args{input: "1234"},
			0,
		},
		{
			"Testing 91212129",
			args{input: "91212129"},
			9,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SumAllDigits(tt.args.input); got != tt.want {
				t.Errorf("SumAllDigits() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSumAllDigits2(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Testing 1212",
			args{input: "1212"},
			6,
		},
		{
			"Testing 1221",
			args{input: "1221"},
			0,
		},
		{
			"Testing 123425",
			args{input: "123425"},
			4,
		},
		{
			"Testing 123123",
			args{input: "123123"},
			12,
		},
		{
			"Testing 12131415",
			args{input: "12131415"},
			4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := SumAllDigits2(tt.args.input)
			if err != nil {
				t.Errorf("SumAllDigits2() return error %e", err)
			}
			if got != tt.want {
				t.Errorf("SumAllDigits2() = %v, want %v", got, tt.want)
			}
		})
	}
}
