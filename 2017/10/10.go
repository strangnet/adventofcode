package aoc2017d10

import "fmt"

var (
	list    []int
	myInput = "83,0,193,1,254,237,187,40,88,27,2,255,149,29,42,100"
)

// Exercise ...
func Exercise() {
	fmt.Println("AOC 2017: Day 7")
	fmt.Println("Running puzzle 1")

	list = initList(256)
}

func initList(size int) []int {
	list := make([]int, size)
	for i := 0; i < 256; i++ {
		list[i] = i
	}
	return list
}

func reverseListSegment(list []int, start int, length int) []int {
	listLength := len(list)
	if start > listLength {
		start = start % listLength
	}

	// var subList []int = make([]int, length)

	return list
}
