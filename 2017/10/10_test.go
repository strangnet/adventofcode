package aoc2017d10

import (
	"reflect"
	"testing"
)

func Test_reverseListSegment(t *testing.T) {
	type args struct {
		list   []int
		start  int
		length int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"Testing 1",
			args{[]int{1, 2, 3, 4}, 0, 4},
			[]int{4, 3, 2, 1},
		},
		{
			"Testing 2",
			args{[]int{1, 2, 3, 4}, 2, 4},
			[]int{4, 3, 2, 1},
		},
		{
			"Testing 3",
			args{[]int{0, 1, 2, 3, 4}, 0, 3},
			[]int{2, 1, 0, 3, 4},
		},

		{
			"Testing 4",
			args{[]int{2, 1, 0, 3, 4}, 3, 4},
			[]int{4, 3, 0, 1, 2},
		},

		{
			"Testing 5",
			args{[]int{4, 3, 0, 1, 2}, 3, 1},
			[]int{4, 3, 0, 1, 2},
		},

		{
			"Testing 6",
			args{[]int{4, 3, 0, 1, 2}, 1, 5},
			[]int{3, 4, 2, 1, 0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := reverseListSegment(tt.args.list, tt.args.start, tt.args.length); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("reverseListSegment() = %v, want %v", got, tt.want)
			}
		})
	}
}
