package main

import (
	"fmt"

	aoc "gitlab.com/strangnet/adventofcode/2017/10"
)

func main() {
	fmt.Println("Advent of Go! 2017")

	aoc.Exercise()
}
