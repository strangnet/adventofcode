package aoc2017d04

import (
	"testing"
)

func TestHasAnagrams(t *testing.T) {
	type args struct {
		p []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Testing 1",
			args{p: []string{"abcde", "fghij"}},
			false,
		},
		{
			"Testing 2",
			args{p: []string{"abcde", "xyz", "ecdab"}},
			true,
		},
		{
			"Testing 3",
			args{p: []string{"a", "ab", "abc", "abd", "abf", "abj"}},
			false,
		},
		{
			"Testing 4",
			args{p: []string{"iiii", "oiii", "ooii", "oooi", "oooo"}},
			false,
		},
		{
			"Testing 5",
			args{p: []string{"oiii", "ioii", "iioi", "iiio"}},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HasAnagrams(tt.args.p); got != tt.want {
				t.Errorf("HasAnagrams() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHasDuplicates(t *testing.T) {
	type args struct {
		p []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Testing 1",
			args{p: []string{"aa", "bb", "cc", "dd", "ee"}},
			false,
		},
		{
			"Testing 2",
			args{p: []string{"aa", "bb", "cc", "dd", "aa"}},
			true,
		},
		{
			"Testing 3",
			args{p: []string{"aa", "bb", "cc", "dd", "aaa"}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HasDuplicates(tt.args.p); got != tt.want {
				t.Errorf("HasDuplicates() = %v, want %v", got, tt.want)
			}
		})
	}
}
