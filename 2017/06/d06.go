package aoc2017d06

import (
	"fmt"
	"strconv"
	"strings"
)

// Exercise runs this day's exercise
func Exercise() {
	fmt.Println("AOC 2017: Day 6")
	fmt.Println("Running puzzle 1")

	m := &Memory{}
	m.init(myInput)
	count, cycles := m.BalanceBank()
	fmt.Printf("It took %d steps to balance the memory.\nCycle was %d steps.\n", count, cycles)
}

func (m *Memory) init(input string) {
	for _, v := range strings.Fields(input) {
		value, _ := strconv.Atoi(v)
		m.add(value)
	}
	m.History = make(map[string]int)
	m.storeState(0)
}

// Memory is the data type holding the memory
type Memory struct {
	Bank    []int
	History map[string]int
}

// BalanceBank balances the memory and returns
// how many steps it took and how large the cycle was, i.e.
// when the current state was recorded previously
func (m *Memory) BalanceBank() (int, int) {
	var count int

	for true {
		// find the largest bank
		max := -1
		index := -1
		for k, v := range m.Bank {
			if v > max {
				max = v
				index = k
			}
		}
		// move all the values to the other banks
		data := m.Bank[index]
		m.Bank[index] = 0
		bankSize := len(m.Bank)
		for data > 0 {
			index++
			m.Bank[index%bankSize]++
			data--
		}

		count++
		fmt.Printf("%#v\n", m.Bank)
		if !m.storeState(count) {
			break
		}
	}
	cycles := count - m.History[m.state()]

	return count, cycles
}

func (m *Memory) state() string {
	var state string
	for _, v := range m.Bank {
		state = state + " " + strconv.Itoa(v)
	}
	strings.Trim(state, " ")
	return state
}

func (m *Memory) storeState(cycle int) bool {
	state := m.state()
	ok := m.History[state]
	if ok != 0 {
		return false
	}
	m.History[state] = cycle
	return true
}

func (m *Memory) add(value int) {
	m.Bank = append(m.Bank, value)
}

var (
	myInput  = "4	1	15	12	0	9	9	5	5	8	7	3	14	5	12	3"
	myInput2 = "0 2 7 0"
)
