package aoc2017d09

import "testing"

func Test_countGroups(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Testing '{}'",
			args{"{}"},
			1,
		},
		{
			"Testing '{{{}}}'",
			args{"{{{}}}"},
			6,
		},
		{
			"Testing '{{},{}}'",
			args{"{{},{}}"},
			5,
		},
		{
			"Testing '{{{},{},{{}}}}'",
			args{"{{{},{},{{}}}}"},
			16,
		},
		{
			"Testing '{<a>,<a>,<a>,<a>}'",
			args{"{<a>,<a>,<a>,<a>}"},
			1,
		},
		{
			"Testing '{{<ab>},{<ab>},{<ab>},{<ab>}}'",
			args{"{{<ab>},{<ab>},{<ab>},{<ab>}}"},
			9,
		},
		{
			"Testing '{{<!!>},{<!!>},{<!!>},{<!!>}}'",
			args{"{{<!!>},{<!!>},{<!!>},{<!!>}}"},
			9,
		},
		{
			"Testing '{{<a!>},{<a!>},{<a!>},{<ab>}}'",
			args{"{{<a!>},{<a!>},{<a!>},{<ab>}}"},
			3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := CountGroups(tt.args.data); got != tt.want {
				t.Errorf("CountGroups() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countCharacters(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Testing '<>'",
			args{"<>"},
			0,
		},
		{
			"Testing '<random characters>'",
			args{"<random characters>"},
			17,
		},
		{
			"Testing '<<<<>'",
			args{"<<<<>"},
			3,
		},
		{
			"Testing '<{!>}>'",
			args{"<{!>}>"},
			2,
		},
		{
			"Testing '<!!>'",
			args{"<!!>"},
			0,
		},
		{
			"Testing '<!!!>>'",
			args{"<!!!>>"},
			0,
		},
		{
			"Testing '<{o\"i!a,<{i<a>'",
			args{"<{o\"i!a,<{i<a>"},
			10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if _, got := CountGroups(tt.args.data); got != tt.want {
				t.Errorf("CountGroups() = %v, want %v", got, tt.want)
			}
		})
	}
}
