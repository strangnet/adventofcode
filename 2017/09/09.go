package aoc2017d09

import (
	"fmt"
	"io/ioutil"
)

const (
	GroupStart   = "{"
	GroupEnd     = "}"
	IgnoreNext   = "!"
	GarbageStart = "<"
	GarbageEnd   = ">"
)

var (
	myInput string
)

func Exercise() {
	fmt.Println("AOC 2017: Day 7")
	fmt.Println("Running puzzle 1")

	readInput(&myInput)
	score, _ := CountGroups(myInput)
	fmt.Printf("Score is %d\n", score)

	fmt.Println("Running puzzle 2")
	_, garbage := CountGroups(myInput)
	fmt.Printf("Garbage is %d\n", garbage)
}

// CountGroups parses the string and counts groups
func CountGroups(data string) (int, int) {
	ignoreNext := false
	inGarbage := false
	groupValueModifier := 0
	score := 0
	garbage := 0
	for _, v := range data {
		currentChar := string(v)
		if ignoreNext {
			ignoreNext = false
			continue
		}
		switch currentChar {
		case IgnoreNext:
			ignoreNext = true
		case GarbageStart:
			if inGarbage {
				garbage++
			} else {
				inGarbage = true
			}
		case GarbageEnd:
			inGarbage = false
		case GroupStart:
			if !inGarbage {
				groupValueModifier++
			} else {
				garbage++
			}
		case GroupEnd:
			if !inGarbage {
				score += groupValueModifier
				groupValueModifier--
			} else {
				garbage++
			}
		default:
			if inGarbage {
				garbage++
			}
		}
	}
	return score, garbage
}

func readInput(input *string) {
	b, _ := ioutil.ReadFile("09/input.txt")
	for _, v := range b {
		*input = *input + string(v)
	}
}
