package main

import (
	"fmt"

	aoc "gitlab.com/strangnet/adventofcode/2018/03"
)

func main() {
	fmt.Println("YO! AoC 2018")

	aoc.Exercise()
}
