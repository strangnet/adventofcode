package aoc2018d03

import "testing"

func TestOverlappingArea(t *testing.T) {
	type args struct {
		patches []string
	}
	tests := []struct {
		name           string
		args           args
		wantOverlapped int
		wantUntouched  int
	}{
		{
			name:           "Test 1",
			args:           args{[]string{"#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"}},
			wantOverlapped: 4,
			wantUntouched:  3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			overlapped, untouched := OverlappingArea(tt.args.patches)
			if got := overlapped; got != tt.wantOverlapped {
				t.Errorf("OverlappingArea() overlapped = %v, wantOverlapped %v", got, tt.wantOverlapped)
			}
			if got := untouched; got != tt.wantUntouched {
				t.Errorf("OverlappingArea() untouched = %v, wantUntouched %v", got, tt.wantUntouched)
			}
		})
	}
}
