package aoc2018d03

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/strangnet/adventofcode/helpers"
)

// Exercise runs the exercise logic
func Exercise() {
	fmt.Println("AoC 2018: Day 03")
	fmt.Println("Running puzzle 1")

	patches := parsePatches()
	overlap, untouched := OverlappingArea(patches)
	fmt.Println(overlap)

	fmt.Println("Running puzzle 2")
	fmt.Println(untouched)

}

type coord struct {
	x int
	y int
}

// OverlappingArea calculates the area of patches that overlap
func OverlappingArea(patches []string) (overlapped int, untouched int) {
	fabricPoints := make(map[coord]int)
	claimsMap := make(map[coord][]int)
	untouchedMap := make(map[int]bool)

	patchMatch := helpers.NewMyRegexp(`#(?P<id>\d+) @ (?P<x>\d+)\,(?P<y>\d+): (?P<w>\d+)x(?P<h>\d+)`)

	for _, patch := range patches {
		res := patchMatch.FindStringSubmatchMap(patch)
		x, _ := strconv.Atoi(res["x"])
		y, _ := strconv.Atoi(res["y"])
		w, _ := strconv.Atoi(res["w"])
		h, _ := strconv.Atoi(res["h"])
		id, _ := strconv.Atoi(res["id"])
		untouchedMap[id] = true
		for i := x; i < x+w; i++ {
			for j := y; j < y+h; j++ {
				c := coord{x: i, y: j}
				fabricPoints[c]++
				l := claimsMap[c]
				if len(l) == 1 {
					untouchedMap[id] = false
					untouchedMap[l[0]] = false
				}
				l = append(l, id)
				claimsMap[c] = l
			}
		}
	}
	for _, v := range fabricPoints {
		if v >= 2 {
			overlapped++
		}
	}
	for i, v := range untouchedMap {
		if v {
			untouched = i
		}
	}

	return
}

func parsePatches() []string {
	var myInput string
	readInput(&myInput)
	patches := strings.Split(myInput, "\n")
	return patches
}

func readInput(input *string) {
	b, _ := ioutil.ReadFile("03/input")
	for _, v := range b {
		*input += string(v)
	}
}
