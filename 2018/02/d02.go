package aoc2018d02

import (
	"fmt"
	"io/ioutil"
	"strings"
)

// Exercise runs the exercise logic
func Exercise() {
	fmt.Println("AoC 2018: Day 02")
	fmt.Println("Running puzzle 1")

	ids := parseIDs()

	checksum := getChecksum(ids)
	fmt.Println(checksum)

	fmt.Println("Running puzzle 2")
	correctID := findCorrectBoxID(ids)
	fmt.Println(correctID)
}

func findCorrectBoxID(ids []string) string {
	seenIDs := make(map[string]bool)
	for _, id := range ids {
		idSplit := strings.Split(id, "")
		currentSeenIDs := make(map[string]bool)
		for i := 0; i < len(idSplit); i++ {
			a := make([]string, len(idSplit))
			a = append(a[:0], idSplit...)
			b := append(a[:i], a[i+1:]...)
			partID := strings.Join(b, "")
			if seenIDs[partID] && !currentSeenIDs[partID] {
				fmt.Printf("id: %s\n", partID)
				return partID
			}
			seenIDs[partID] = true
			currentSeenIDs[partID] = true
		}
	}
	return ""
}

func parseIDs() []string {
	var myInput string
	readInput(&myInput)
	ids := strings.Split(myInput, "\n")
	return ids
}

func getChecksum(ids []string) int {
	var twice, thrice int
	for _, id := range ids {
		t1, t2 := MultiCount(id)
		if t1 {
			twice++
		}
		if t2 {
			thrice++
		}
	}
	return CalculateChecksum(twice, thrice)
}

// MultiCount counts if any letters in a string shows multiple times
// Return bool for if any letter shows twice or three times respectively
func MultiCount(id string) (twice bool, thrice bool) {
	letterCount := make(map[string]int)
	idSplit := strings.Split(id, "")
	for _, letter := range idSplit {
		letterCount[letter]++
	}
	values := mapValues(letterCount)
	for _, v := range values {
		if v == 2 {
			twice = true
		}
		if v == 3 {
			thrice = true
		}
	}
	return
}

// CalculateChecksum creates a checksum from the count of id:s
// with letters appearing twice and three times
func CalculateChecksum(twice int, thrice int) int {
	return twice * thrice
}

func mapValues(input map[string]int) []int {
	values := []int{}
	for _, value := range input {
		values = append(values, value)
	}
	return values
}

func readInput(input *string) {
	b, _ := ioutil.ReadFile("02/input")
	for _, v := range b {
		*input += string(v)
	}
}
