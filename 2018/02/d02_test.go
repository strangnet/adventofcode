package aoc2018d02

import (
	"testing"
)

func TestMultiCount(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name       string
		args       args
		wantTwice  bool
		wantThrice bool
	}{
		{
			name:       "Test 1",
			args:       args{id: "abcdef"},
			wantTwice:  false,
			wantThrice: false,
		},
		{
			name:       "Test 2",
			args:       args{id: "bababc"},
			wantTwice:  true,
			wantThrice: true,
		},
		{
			name:       "Test 3",
			args:       args{id: "abbcde"},
			wantTwice:  true,
			wantThrice: false,
		},
		{
			name:       "Test 4",
			args:       args{id: "abcccd"},
			wantTwice:  false,
			wantThrice: true,
		},
		{
			name:       "Test 5",
			args:       args{id: "aabcdd"},
			wantTwice:  true,
			wantThrice: false,
		},
		{
			name:       "Test 6",
			args:       args{id: "abcdee"},
			wantTwice:  true,
			wantThrice: false,
		},
		{
			name:       "Test 7",
			args:       args{id: "ababab"},
			wantTwice:  false,
			wantThrice: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTwice, gotThrice := MultiCount(tt.args.id)
			if gotTwice != tt.wantTwice {
				t.Errorf("MultiCount() gotTwice = %v, want %v", gotTwice, tt.wantTwice)
			}
			if gotThrice != tt.wantThrice {
				t.Errorf("MultiCount() gotThrice = %v, want %v", gotThrice, tt.wantThrice)
			}
		})
	}
}

func TestCalculateChecksum(t *testing.T) {
	type args struct {
		twice  int
		thrice int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{twice: 4, thrice: 3},
			want: 12,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CalculateChecksum(tt.args.twice, tt.args.thrice); got != tt.want {
				t.Errorf("CalculateChecksum() = %v, want %v", got, tt.want)
			}
		})
	}
}
