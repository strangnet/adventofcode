package aoc2018d01

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

// Exercise runs the exercise logic
func Exercise() {
	fmt.Println("AoC 2018: Day 01")
	fmt.Println("Running puzzle 1")

	chng := SumAllFrequencyChanges()
	fmt.Println(chng)

	fmt.Println("Running puzzle 2")
	firstFreq := FindFirstFrequency()
	fmt.Println(firstFreq)
}

// SumAllFrequencyChanges calculates the final frequency
func SumAllFrequencyChanges() int {
	var myInput string
	readInput(&myInput)
	freqChanges := strings.Split(myInput, "\n")
	parsed := ParseFrequencyChanges(freqChanges)
	return parsed
}

// FindFirstFrequency finds the first frequency that the device reaches twice
func FindFirstFrequency() int {
	var myInput string
	readInput(&myInput)
	freqChanges := strings.Split(myInput, "\n")
	firstFreq := ParseAndFindFrequencyChanges(0, freqChanges)
	return firstFreq
}

// ParseAndFindFrequencyChanges parses input and finds first frequency that is reached twice
func ParseAndFindFrequencyChanges(initFreq int, changes []string) int {
	freq := initFreq
	var reachedFrequencies = make(map[int]bool)
	reachedFrequencies[0] = true
	changeMatch := regexp.MustCompile(`([+-])(\d+)`)
	sanity := 200
	count := 0
	for {
		for _, change := range changes {
			res := changeMatch.FindStringSubmatch(change)
			op := res[1]
			v, _ := strconv.Atoi(res[2])
			switch op {
			case "+":
				freq += v
			case "-":
				freq -= v
			}
			if reachedFrequencies[freq] {
				return freq
			}

			reachedFrequencies[freq] = true

		}
		count++
		if count >= sanity {
			break
		}
	}
	return 0
}

// ParseFrequencyChanges convert the raw input to a sum
func ParseFrequencyChanges(changes []string) int {
	changeMatch := regexp.MustCompile(`([+-])(\d+)`)
	sum := 0
	for _, change := range changes {
		res := changeMatch.FindStringSubmatch(change)
		op := res[1]
		v, _ := strconv.Atoi(res[2])
		switch op {
		case "+":
			sum += v
		case "-":
			sum -= v
		}
	}
	return sum
}

func readInput(input *string) {
	b, _ := ioutil.ReadFile("01/input")
	for _, v := range b {
		*input += string(v)
	}
}
