package aoc2018d01

import (
	"testing"
)

func TestParseFrequencyChanges(t *testing.T) {
	type args struct {
		input []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Test 1",
			args{input: []string{"+1", "+1", "+1"}},
			3,
		},
		{
			"Test 2",
			args{input: []string{"+1", "+1", "-2"}},
			0,
		},
		{
			"Test 3",
			args{input: []string{"-1", "-2", "-3"}},
			-6,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseFrequencyChanges(tt.args.input); got != tt.want {
				t.Errorf("ParseFrequencyChanges() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseAndFindFrequencyChanges(t *testing.T) {
	type args struct {
		initFreq int
		changes  []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Test 1",
			args{initFreq: 0, changes: []string{"+1", "-1"}},
			0,
		},
		{
			"Test 2",
			args{initFreq: 0, changes: []string{"+3", "+3", "+4", "-2", "-4"}},
			10,
		},
		{
			"Test 3",
			args{initFreq: 0, changes: []string{"-6", "+3", "+8", "+5", "-6"}},
			5,
		},
		{
			"Test 4",
			args{initFreq: 0, changes: []string{"+7", "+7", "-2", "-7", "-4"}},
			14,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseAndFindFrequencyChanges(tt.args.initFreq, tt.args.changes); got != tt.want {
				t.Errorf("ParseAndFindFrequencyChanges() = %v, want %v", got, tt.want)
			}
		})
	}
}
