package helpers

import (
	"regexp"
)

type MyRegexp struct {
	*regexp.Regexp
}

func NewMyRegexp(s string) MyRegexp {
	return MyRegexp{regexp.MustCompile(s)}
}

func (r *MyRegexp) FindStringSubmatchMap(s string) map[string]string {
	captures := make(map[string]string)

	match := r.FindStringSubmatch(s)
	if match == nil {
		return captures
	}

	for i, name := range r.SubexpNames() {
		//
		if i == 0 {
			continue
		}
		captures[name] = match[i]

	}
	return captures
}
