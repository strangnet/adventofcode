require "digest/md5"

input = "reyedfim"

code = [nil,nil,nil,nil,nil,nil,nil,nil]
found = 0

i = 0
until found == 8
  hash = Digest::MD5.hexdigest(input+i.to_s)
  if hash.match(/^00000/)
    if code[hash[5].to_i] == nil and hash[5].match(/[0-7]/)
      code[hash[5].to_i] = hash[6]
      found += 1
    end
  end
  i += 1
end

puts code.join
