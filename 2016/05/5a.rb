require "digest/md5"

input = "reyedfim"

code = ""

i = 0
until code.size == 8
  hash = Digest::MD5.hexdigest(input+i.to_s)
  if hash.match(/^00000/)
    code += hash[5]
  end
  i += 1
end

puts code
