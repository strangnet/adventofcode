package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

const (
	inputFile = "input.txt"
)

type bot struct {
	chips []int
}

var (
	bots   = make(map[int]bot)
	output = make(map[int]int)
)

func readFileByRow() ([]string, error) {
	var rows []string
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		return nil, err
	}
	rows = strings.Split(strings.TrimSpace(string(input)), "\n")

	return rows, nil
}

func readInput(rows []string) {
	//                                1         2        3          4               5         6     7      8              9        10
	regCmd := regexp.MustCompile(`(bot|value) (\d+) (goes|gives (low|high)) to (bot|output) (\d+)( and (low|high) to (bot|output) (\d+))?`)
	for _, cmd := range rows {
		match := regCmd.FindStringSubmatch(cmd)
		fmt.Println(cmd, " l:", len(match))

		if match != nil {

			switch match[1] {
			case "value":
				botNr, _ := strconv.Atoi(match[4])
				value, _ := strconv.Atoi(match[2])
				add(value, botNr)
			case "bot":
				fromBot, _ := strconv.Atoi(match[2])
				to1, _ := strconv.Atoi(match[6])
				toType1 := match[5]
				to2, _ := strconv.Atoi(match[10])
				toType2 := match[9]
				switch toType1 {
				case "bot":
					switch match[4] {
					case "low":
						fmt.Println(fromBot)
						add(bots[fromBot].low(), to1)
					case "high":
						add(bots[fromBot].high(), to1)
					}

				case "output":
					switch match[4] {
					case "low":
						output[to1] = bots[fromBot].low()
					case "high":
						output[to1] = bots[fromBot].high()
					}
				}
				switch toType2 {
				case "bot":
					switch match[8] {
					case "low":
						add(bots[fromBot].low(), to2)
					case "high":
						add(bots[fromBot].high(), to2)
					}

				case "output":
					switch match[4] {
					case "low":
						output[to2] = bots[fromBot].low()
					case "high":
						output[to2] = bots[fromBot].high()
					}
				}

				clear(fromBot)
			}
		}
	}
}

func (b bot) low() int {
	if b.chips[0] < b.chips[1] {
		return b.chips[0]
	}
	return b.chips[1]
}

func (b bot) high() int {
	if b.chips[0] < b.chips[1] {
		return b.chips[1]
	}
	return b.chips[0]
}

func clear(botNr int) {
	bo := bots[botNr]
	bo.chips = nil
	bots[botNr] = bo
}

func add(val int, botNr int) {
	var b bot
	if _, ok := bots[botNr]; !ok {
		bots[botNr] = bot{}
		b = bots[botNr]
	}
	if len(b.chips) > 2 {
		panic("Overflow detected")
	}
	b.chips = append(b.chips, val)
	bots[botNr] = b
}

func main() {
	rows, err := readFileByRow()
	if err != nil {
		panic(err)
	}

	readInput(rows)
}
