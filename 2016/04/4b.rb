file = File.read("rooms4.txt")
rows = file.split("\n")

$alphabet = "abcdefghijklmnopqrstuvwxyz"

test_rooms = [
  "qzmt-zixmtkozy-ivhz-343[zimthk]",
  "aaaaa-bbb-z-y-x-123[abxyz]",
  "a-b-c-d-e-f-g-h-987[abcde]",
  "not-a-real-room-404[oarel]",
  "totally-real-room-200[decoy]"
]

def validate_checksum(enc, checksum)
  return true if calculate_checksum(enc).start_with?(checksum)
end

def calculate_checksum(enc)
  letters = enc.delete("-")
  l_hash = Hash.new(0)
  letters.each_char do |ch|
    next unless ch =~ /\w/
    l_hash[ch] += 1
  end
  sorted = l_hash.sort_by { |a,b| b}.reverse
  sorted = sorted.sort do |a,b|
    if a[1] > b[1]
      -1
    elsif a[1] < b[1]
      1
    else
      a[0] <=> b[0]
    end
  end
  sorted.collect {|a| a[0]}.join
end

def decrypt_room(enc, sector)
  name = ""
  enc.each_char do |a|
    if a == "-"
      name += " "
    else
      name += $alphabet[($alphabet.index(a)+sector) % $alphabet.size]
    end
  end
  name
end

def get_sector(room,input)
  input.each do |line|
    line_matching = /([a-z\\-]+)-(\d+)\[(\w+)\]/.match(line)
    enc = line_matching[1]
    sector = line_matching[2].to_i
    checksum = line_matching[3]
    if validate_checksum(enc, checksum)
      room_name = decrypt_room(enc, sector)
      return sector if room_name.start_with?(room)
    end
  end
end

puts get_sector("northpole", rows)
