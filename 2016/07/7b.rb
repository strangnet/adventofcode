file = File.read("input.txt")
rows = file.split("\n")

def has_aba(matches)
  res = []
  matches.flatten.each do |str|
    1.upto(str.size-1) do |i|
      if str[i-1] == str[i+1] and str[i] != str[i+1]
        res.push("#{str[i-1]}#{str[i]}#{str[i+1]}")
      end
    end
  end
  res
end

def has_bab(corresponding, matches)
  trans = []
  corresponding.each do |corr|
    transversed = "#{corr[1]}#{corr[0]}#{corr[1]}"
    trans.push(transversed)
  end
  # puts "trans: #{trans}"
  res = []
  matches.flatten.each do |str|
    1.upto(str.size-1) do |i|
      if str[i-1] == str[i+1] and str[i] != str[i+1]
        res.push("#{str[i-1]}#{str[i]}#{str[i+1]}")
      end
    end
  end
  # puts "res: #{res}"
  res & trans
end

def validate_ssl(addr)
  pre_brackets = has_aba(addr.scan(/(\w+)\[/))
  post_brackets = has_aba(addr.scan(/\](\w+)$/))
  supernet = pre_brackets.concat(post_brackets)
  # puts "supernet: #{supernet}"
  is_ssl = has_bab(supernet,addr.scan(/\[(\w+)\]/))
  # puts "is_ssl: #{is_ssl}"

  if is_ssl.size > 0
    true
  else
    false
  end
end

def ssl_count(rows)
  valid_ssl_addresses = 0
  rows.each do |row|
    valid_ssl_addresses += 1 if validate_ssl(row)
  end
  valid_ssl_addresses
end

puts ssl_count(rows)
