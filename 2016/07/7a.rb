file = File.read("input.txt")
rows = file.split("\n")

def has_abba(matches)
  matches.flatten.each do |str|
    2.upto(str.size-1) do |i|
      if str[i-2] == str[i+1] and str[i-1] == str[i] and str[i] != str[i+1]
        return true
      end
    end
  end
  false
end

def validate_tls(addr)
  pre_brackets = has_abba(addr.scan(/(\w+)\[/))
  inside_brackets = has_abba(addr.scan(/\[(\w+)\]/))
  post_brackets = has_abba(addr.scan(/\](\w+)$/))
  if not inside_brackets and (pre_brackets or post_brackets)
    true
  else
    false
  end
end

def tls_count(rows)
  valid_tls_addresses = 0
  rows.each do |row|
    valid_tls_addresses += 1 if validate_tls(row)
  end
  valid_tls_addresses
end

puts tls_count(rows)
