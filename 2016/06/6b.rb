file = File.read("input.txt")
rows = file.split("\n")

columns = [
  Hash.new(0),
  Hash.new(0),
  Hash.new(0),
  Hash.new(0),
  Hash.new(0),
  Hash.new(0),
  Hash.new(0),
  Hash.new(0)
]

rows.each do |row|
  i = 0
  row.each_char do |ch|
    columns[i][ch] += 1
    i += 1
  end
end
0..8.times {|i| puts columns[i].min_by {|a,b| b} }
