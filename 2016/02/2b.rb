file = File.read("keypad.txt")
lines = file.split("\n")

$keypad = Hash.new
$keypad["1"] = {"U" => nil, "R" => nil, "D" => "3", "L" => nil}
$keypad["2"] = {"U" => nil, "R" => "3", "D" => "6", "L" => nil}
$keypad["3"] = {"U" => "1", "R" => "4", "D" => "7", "L" => "2"}
$keypad["4"] = {"U" => nil, "R" => nil, "D" => "8", "L" => "3"}
$keypad["5"] = {"U" => nil, "R" => "6", "D" => nil, "L" => nil}
$keypad["6"] = {"U" => "2", "R" => "7", "D" => "A", "L" => "5"}
$keypad["7"] = {"U" => "3", "R" => "8", "D" => "B", "L" => "6"}
$keypad["8"] = {"U" => "4", "R" => "9", "D" => "C", "L" => "7"}
$keypad["9"] = {"U" => nil, "R" => nil, "D" => nil, "L" => "8"}
$keypad["A"] = {"U" => "6", "R" => "B", "D" => nil, "L" => nil}
$keypad["B"] = {"U" => "7", "R" => "C", "D" => "D", "L" => "A"}
$keypad["C"] = {"U" => "8", "R" => nil, "D" => nil, "L" => "B"}
$keypad["D"] = {"U" => "B", "R" => nil, "D" => nil, "L" => nil}

$last_key = "5"
$code = ""

def next_key(prev, mov)
  k = prev
  n = $keypad[k]
  if n[mov]
    k = n[mov]
  end
  k
end
lines.each do |line|
  chars = line.split('')
  k = $last_key
  chars.each do |ch|
    k = next_key(k, ch)
  end
  $last_key = k
  $code += $last_key
end

puts $code
