file = File.read("keypad.txt")
lines = file.split("\n")

$last_key = 5
$code = ""

def next_key(prev, mov)
  k = prev
  case mov
  when "U"
    k -= 3 if k > 3
  when "L"
    k -= 1 if (k != 1 and k != 4 and k != 7)
  when "R"
    k += 1 if (k != 6 and k != 6 and k != 9)
  when "D"
    k += 3 if k < 7
  end
  k
end


lines.each do |line|
  chars = line.split('')
  k = $last_key
  chars.each do |ch|
    k = next_key(k, ch)
  end
  $last_key = k
  $code += $last_key.to_s
end

puts $code
