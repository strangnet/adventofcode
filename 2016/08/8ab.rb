require 'matrix'
file = File.read("input.txt")
rows = file.split("\n")

class Matrix
  def []=(i, j, x)
    @rows[i][j] = x
  end

  def to_s
    out = ""

    0.upto(self.row_count) do |ri|
      0.upto(self.column_count) do |ci|
        out += self[ri,ci].to_s
      end
      out += "\n"
    end

    out
  end
end

$width = 50
$height = 6
$screen = Matrix.build(6, 50) { "." }

def normalized_idx(idx, max)
  idx % max
end

def rect(cols, rows)
  0.upto(rows-1) do |row|
    0.upto(cols-1) do |col|
      $screen[row, col] = "#"
    end
  end
end

def rotate_row(idx, amount)
  row = $screen.row(idx)
  0.upto($width-1) do |col|
    norm_idx = normalized_idx(col+amount, $width)
    $screen[idx, norm_idx] = row[col]
  end
end

def rotate_col(idx, amount)
  col = $screen.column(idx)
  0.upto($height-1) do |row|
    norm_idx = normalized_idx(row+amount, $height)
    $screen[norm_idx, idx] = col[row]
  end
end

rows.each do |row|
  parsed = row.match(/(rect|rotate)(.*)/)
  command = parsed[1]
  arguments = parsed[2].strip
  case command
  when "rect"
    args = arguments.match(/(\d+)x(\d+)/)
    rect(args[1].to_i, args[2].to_i)
  when "rotate"
    args = arguments.match(/(row|column) (x|y)=(\d+) by (\d+)/)
    rc = args[1]
    idx = args[3].to_i
    amount = args[4].to_i
    case rc
    when "row"
      rotate_row(idx, amount)
    when "column"
      rotate_col(idx, amount)
    end
  end
end

puts $screen
count = Hash.new 0
$screen.each {|i| count[i]+=1}
puts count["#"]
