package main

import "testing"

func Test_expandStringWithMarker(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"ADVENT", "ADVENT"},
		{"A(1x5)BC", "ABBBBBC"},
		{"(3x3)XYZ", "XYZXYZXYZ"},
		{"A(2x2)BCD(2x2)EFG", "ABCBCDEFEFG"},
		{"(6x1)(1x3)A", "(1x3)A"},
		{"X(8x2)(3x3)ABCY", "X(3x3)ABC(3x3)ABCY"},
	}

	for _, tt := range tests {
		o, err := expandStringWithMarker(tt.in)
		if err != nil {
			t.Errorf("expand(%q) = error %s, want %q", tt.in, err, tt.out)
		} else if o != tt.out {
			t.Errorf("expand(%q) = %q, want %q", tt.in, o, tt.out)
		}
	}
}
