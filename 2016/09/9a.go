package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

const (
	inputFile = "input.txt"
)

func main() {
	file, err := ioutil.ReadFile(inputFile)
	if err != nil {
		panic(err)
	}

	fmt.Println("Processing version 1:")
	length, err := processFile(strings.TrimSpace(string(file)))
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	fmt.Printf("Decompressed length = %v\n", length)

	fmt.Println("Processing version 2:")
	length, err = processFile2(strings.TrimSpace(string(file)))
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	fmt.Printf("Decompress length = %v\n", length)
}

var markerMatch = regexp.MustCompile("\\(([0-9]+)x([0-9]+)\\)")

func expandStringWithMarker(input string) (string, error) {
	var out string

	for len(input) > 0 {
		match := markerMatch.FindStringSubmatchIndex(input)
		if match == nil {
			out += input
			input = ""
		} else {
			out += input[:match[0]]
			l, err := strconv.Atoi(input[match[2]:match[3]])
			if err != nil {
				return "", err
			}
			c, err := strconv.Atoi(input[match[4]:match[5]])
			if err != nil {
				return "", err
			}
			out += strings.Repeat(input[match[1]:match[1]+l], c)
			input = input[match[1]+l:]
		}
	}

	return out, nil
}

func expandStringWithMarker2(input string) (int, error) {
	var length int

	for len(input) > 0 {
		match := markerMatch.FindStringSubmatchIndex(input)
		if match == nil {
			length += len(input)
			input = ""
		} else {
			length += match[0]
			l, err := strconv.Atoi(input[match[2]:match[3]])
			if err != nil {
				return 0, err
			}
			c, err := strconv.Atoi(input[match[4]:match[5]])
			if err != nil {
				return 0, nil
			}
			rep := input[match[1] : match[1]+l]
			repLength, err := expandStringWithMarker2(rep)
			if err != nil {
				return 0, nil
			}
			length += repLength * c
			input = input[match[1]+l:]
		}
	}

	return length, nil
}

func processFile(input string) (int, error) {
	out, err := expandStringWithMarker(input)
	return len(out), err
}

func processFile2(input string) (int, error) {
	length, err := expandStringWithMarker2(input)
	return length, err
}
