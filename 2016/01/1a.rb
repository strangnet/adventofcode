input = "R2, L5, L4, L5, R4, R1, L4, R5, R3, R1, L1, L1, R4, L4, L1, R4, L4, R4, L3, R5, R4, R1, R3, L1, L1, R1, L2, R5, L4, L3, R1, L2, L2, R192, L3, R5, R48, R5, L2, R76, R4, R2, R1, L1, L5, L1, R185, L5, L1, R5, L4, R1, R3, L4, L3, R1, L5, R4, L4, R4, R5, L3, L1, L2, L4, L3, L4, R2, R2, L3, L5, R2, R5, L1, R1, L3, L5, L3, R4, L4, R3, L1, R5, L3, R2, R4, R2, L1, R3, L1, L3, L5, R4, R5, R2, R2, L5, L3, L1, L1, L5, L2, L3, R3, R3, L3, L4, L5, R2, L1, R1, R3, R4, L2, R1, L1, R3, R3, L4, L2, R5, R5, L1, R4, L5, L5, R1, L5, R4, R2, L1, L4, R1, L1, L1, L5, R3, R4, L2, R1, R2, R1, R1, R3, L5, R1, R4"

arr = input.split(',')

$horiz = 0
$vert = 0
$weights = [-1, 1, 1, -1]
$facing = 1
$steps = 0

def move(direction, amount)
  if direction == 'L'
    correction = -1
  elsif direction == 'R'
    correction = 1
  end
  $facing = $facing + correction
  $facing = $facing + 4 if $facing < 0
  $facing = $facing - 4 if $facing > 3
  if $facing % 2 == 0
    $horiz = $horiz + (amount * $weights[$facing])
  else
    $vert = $vert + (amount * $weights[$facing])
  end
end

arr.each do |move|
  parsed = /([LR])(\d+)/.match(move)
  dir = parsed[1]
  amount = parsed[2]

  move(dir, amount.to_i)
end

puts $horiz.abs() + $vert.abs()
