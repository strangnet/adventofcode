file = File.read("triangles.txt")
rows = file.split("\n")

tri = 0

rows.each do |line|
  clean = line.split(/\s+/)
  if clean.size == 3
    clean.sort_by!(&:to_i)
    if clean[0].to_i + clean[1].to_i > clean[2].to_i
      tri += 1
    end
  end
end

puts tri
