file = File.read("triangles.txt")
rows = file.split("\n")

tri = 0

i = 0
while i < rows.size
  row1 = rows[i].split(/\s+/)
  row2 = rows[i+1].split(/\s+/)
  row3 = rows[i+2].split(/\s+/)
  for x in 0..2
    triangle = [row1[x], row2[x], row3[x]]
    triangle.sort_by!(&:to_i)
    if triangle[0].to_i + triangle[1].to_i > triangle[2].to_i
      tri += 1
    end
  end
  i += 3
end

puts tri
